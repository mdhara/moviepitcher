class Movie < ActiveRecord::Base
  attr_accessible :date_of_rel, :name, :poster_url
  has_many :review
end
