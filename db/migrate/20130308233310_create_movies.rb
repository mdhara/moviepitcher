class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :name
      t.date :date_of_rel
      t.string :poster_url

      t.timestamps
    end
  end
end
